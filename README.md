# Install XMonad in Void Linux

## Instructions

* git clone the repo
* cd to the dir
```
cd xmonad-void/
```
* change permissions
```
chmod +x installer.sh
```
* run the shell file
```
./installer.sh
```
